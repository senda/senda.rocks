---
titulo: Gestionando una colección de música
categoria: aplicaciones
descripcion: <p>Con el paso de los años, y un poco de Diógenes digital, he ido acumulando una colección interesante de música</p><p>Soy bastante maniática y algo que me desespera un poco es que cada aplicación, web, servicio o tienda etiquete los ficheros de una forma. Así que he tenido que buscarme una forma de mantenerlo todo ordenado y al alcance, sobre todo ahora que me he retirado de los servicios de streaming</p>
pubDate: 2024-05-03T20:44:00
tags:
  - música
  - metatags
  - mp3
  - scrobbing
  - reproductor
---
Nunca se me olvidará el momento en el que un fallo de nada en un disco duro hiciera que la única copia de mi música que tuviese, estuviera dentro de un iPod Classic. Bien por el iPod, mal por Apple que convirtió lo que era una colección preciosa en una amalgama de carpetas y ficheros con nombres aleatorios (odio eterno a iTunes, no sé si sigue haciendo este tipo de guarradas).

Por si te apetece saltar directamente a los enlaces y comprobar por tu cuenta las aplicaciones, te dejo un [Resumen](#resumen)


## Picard al rescate

Va, me voy a ahorrar el chascarrillo con Star Trek y vamos a ir a lo que nos importa ahora mismo.

[Picard](https://picard.musicbrainz.org/) es una aplicación multiplataforma que nos permite editar los metadatos de nuestros ficheros de audio, descargar y embeber las carátulas. Además, podemos definir una estructura para crear carpetas acorde y renombrar los ficheros.

![Picard](picard_demo_500.png)

Para agregar los metadatos a los ficheros, Picard utiliza la base de datos de [MusicBrainz](https://musicbrainz.org/), una biblioteca abierta de metadatos de música con un API que nos permite desarrollar aplicaciones que utilicen esta información.

Una cosa importante es que, para que pueda trabajar, necesitamos que los ficheros tengan algún tipo de información. En última instancia disponemos de la opción de AcousticID que utilizará la propia música (una huella digital única asociada) para tratar de localizar el tema.

En mi caso, la aplicación no me ha fallado nunca, eso sí, está pensada para que supervises los datos que descarga. Por ejemplo, un mismo disco puede tener diferente número u orden de canciones dependiendo de la edición que seleccionemos, así que nos tocará ir revisando y haciendo algunas modificaciones. Hay un [paso a paso en la web](https://picard.musicbrainz.org/quick-start/) que puede ayudarte si no te haces con la herramienta a la primera.

Como mi colección sigue viva y creciendo, el flujo que sigo para agregar nuevos discos es:

1. Todos los discos nuevos van a una carpeta de entrada, separada de la colección.
2. En Picard he configurado que los ficheros se almacenen en la carpeta de destino con la estructura `Autor > Disco > XX título canción`
3. Importo la carpeta con los ficheros nuevos en la aplicación, reviso que toda la información descargada sea correcta y guardo los cambios.

Y bueno, un rato entretenido más tarde, ya tengo toda mi colección en la carpeta en la que quiero tenerla, con la estructura que quiero, los metatag actualizados y todo en orden. Llega el momento de escucharla.

## Strawberry Music Player

Después de lidiar con el "destrozo" que había hecho iTunes con mi colección, lo que tenía claro es que no quería que mi reproductor importase o modificase de ninguna forma mis carpetas. 

Hace tiempo, antes de Spotify, usaba mucho [Amarok](https://amarok.kde.org/es.html) en Linux, en el momento en que fui a buscarla, llevaba varios años sin actualizarse así que seguí buscando (En abril de 2024 lanzaron la v3.0, quizá le de una vuelta más adelante). La siguiente parada, por lógica, era [Clementine](https://www.clementine-player.org/es/), un fork de Amarok 1.4 que iba bastante bien, pero nada, sin actualizar desde 2016. 

Y llegué a [Strawberry](https://www.strawberrymusicplayer.org), que es un fork de Clementine, así que al final, con algunos rodeos terminé descargándome la misma aplicación que usaba hace unos cuantos años.

![Pantalla de la aplicación Strawberry](screenshot-014-large.png)

Strawberry tiene las siguientes características fundamentales:

- Soporte para formatos WAV, FLAC, WavPack, Ogg Vorbis, Speex, MPC, TrueAudio, AIFF, MP4, MP3, ASF y Monkey's Audio
- Creación de listas de reproducción dinámicas e inteligentes.
- Descarga las carátulas desde [Last.fm](https://www.last.fm/), [Musicbrainz](https://musicbrainz.org/), [Discogs](https://www.discogs.com/), [Musixmatch](https://www.musixmatch.com/), [Deezer](https://www.deezer.com/), [Tidal](https://www.tidal.com/) y [Spotify](https://www.spotify.com/)
- Letras de canciones de [Genius](https://genius.com/), [Musixmatch](https://www.musixmatch.com/), [ChartLyrics](http://www.chartlyrics.com/), [lyrics.ovh](https://lyrics.ovh/), [lololyrics.com](https://www.lololyrics.com/), [songlyrics.com](https://www.songlyrics.com/), [azlyrics.com](https://www.azlyrics.com/), [elyrics.net](https://www.elyrics.net/) y [lyricsmode.com](https://www.lyricsmode.com/)
- Scrobbler con soporte para [Last.fm](https://www.last.fm/), [Libre.fm](https://libre.fm/) y [ListenBrainz](https://listenbrainz.org/); a través de este último también soporta la aplicación web autoalojable [Maloja](https://github.com/krateng/maloja).

Como ya os habréis imaginado, me he hecho una cuenta de ListenBrainz para hacer scrobbing y olvidarme de Last.fm.
## ListenBrainz 

[ListenBrainz](https://listenbrainz.org) es parte de la misma fundación que MusicBrainz y Picard. 

En lo básico, te deja almacenar tu historial de escucha, ver el de otras personas y recibir sugerencias. Por debajo toda la información que recoge se libera con licencia CC0, equivalente al dominio público. 

Para lo que yo quiero usarlo, me sobra, la web está pidiendo a gritos que la traigan al siglo XXI, pero mientras resucita o no [Libre.fm](https://libre.fm/), tendrá que servir. Puede que me líe la manta a la cabeza y me monte un Maloja, ya te iré contando.

## Resumen

Esta es mi forma de gestionar mi colección de música, te dejo una tabla resumiendo las aplicaciones.

| Acción                                           | Aplicación/Servicio                                 |
| ------------------------------------------------ | --------------------------------------------------- |
| Editar metatags, renombrar y ordenar en carpetas | [Picard](https://picard.musicbrainz.org/)           |
| Ver mi colección y escuchar música               | [Strawberry](https://www.strawberrymusicplayer.org) |
| Scrobbing (almacenar historial de escuchas)      | [ListenBrainz](https://listenbrainz.org)            |
| Scrobbing (self-hosted)                          | [Maloja](https://github.com/krateng/maloja).<br>    |



