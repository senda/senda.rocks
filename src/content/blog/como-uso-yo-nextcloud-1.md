---
titulo: Como uso yo NextCloud (Parte 1)
categoria: self-host
descripcion: <p>Cuando me decidí a instalar Nextcloud en un VPS, lo hice principalmente para sustituir a otros servicios que utilizaba para sincronizar ficheros entre dispositivos y para hacer copias de seguridad de las fotos.</p><p>Sin duda, para todo eso me ha ido genial, pero poco a poco he empezado a usarlo para sincronizar otras cosas y salirme de otras aplicaciones.</p>
pubDate: 2024-05-01T19:38:00
tags:
  - cloud
  - sincronización
  - tareas
  - calendario
  - fotos
  - marcadores
  - colaboración
  - nextcloud
  - linux
  - android
---
## ¿Qué es NextCloud?

[NextCloud ](https://nextcloud.com/)o NextCloud Hub como lo llaman en la página oficial, es una suite de colaboración y sincronización online.

NextCloud integra diferentes herramientas, siendo las principales:
- Almacenamiento y sincronización de archivos.
- Groupware: calendario, contactos, cliente de email.
- Chats y videoconferencia privada.
- Herramientas ofimáticas.

Además, podemos instalar numerosas aplicaciones adicionales fácilmente desde la propia interfaz web y seguir ampliando la funcionalidad.

NextCloud es software libre y, por supuesto, lo puedes alojar en tu propio espacio (también puedes contratar proveedores que lo alojen por ti, si el bricolaje informático no es lo tuyo).

Si quieres saltar directamente a ver las aplicaciones que uso, puedes irte al [resumen](#resumen).
## ¿Cómo uso yo NextCloud?

Como comentaba antes, mi llegada a NextCloud vino dada por la necesidad de abandonar Google Fotos y Dropbox, así que la principal utilidad que le doy es la de la sincronización y copia de seguridad de ficheros.

En mi caso, tengo una instancia autoalojada. Puedes descargar los servers desde https://nextcloud.com/install/#instructions-server, pero cómo instalarlos y configurarlos queda fuera del ámbito de este artículo.

Te cuento un poco como uso cada aplicación, tanto en web como en mis diferentes dispositivos. Si te apetece saltar directamente a los enlaces, te dejo un [[#Resumen]].
### Sincronización de archivos y carpetas entre dispositivos

![NextCloud](nextcloud-hub7-files-preview.webp)

Para empezar a sincronizar archivos, debemos descargarnos las aplicaciones correspondientes a nuestras plataformas desde: https://nextcloud.com/install/#install-clients

La sincronización entre dispositivos de escritorio es completa, seleccionamos un directorio local y lo apuntamos a un directorio del servidor, ya existente o que crearemos mientras hacemos la configuración. Con esto ya tendremos una conexión de sincronización activa, muy del estilo de las de Dropbox.

Con las aplicaciones para móvil nos encontramos con las mismas limitaciones que tienen el resto, no se trata de una sincronización sino de la posibilidad de establecer carpetas del dispositivo que se subirán automáticamente al servidor (generalmente las carpetas de la cámara o las fotos que recibimos), subir archivos puntuales y descargar o visualizar archivos de forma individual.
### Mejorar la visualización de las fotos

Una de las cosas que me gustaban de Google Fotos eran los recordatorios de fotos, las fotos en un mapa... vamos todas las "tonterias".

Para mejorar la experiencia de navegación de las fotos en NextCloud, tengo instalado [Memories](https://memories.gallery/)

![Memories](memories.webp)

Con Memories puedo:

- Ver mis fotos en una línea de tiempo.
- Ir a cualquier fecha de forma inmediata.
- Crear álbumes y compartirlos.
- Editar los metadatos
- Archivar fotos para que no aparezcan en mi línea de tiempo.
- Ver las fotos en un mapa.
- Etiquetado automático (que no he probado demasiado).

Memories tiene aplicación para Android: [Memories](https://f-droid.org/es/packages/gallery.memories/)
### Calendarios

![Calendar](calendar_application.png)

Una de las características básicas de NextCloud, habíamos comentado que era la de los [Calendarios](https://github.com/nextcloud/calendar/)

Puedo crear y editar calendarios cómodamente desde la interfaz web o sincronizarlos con mis aplicaciones. También puedo invitar personas, crear recordatorios o visualizar calendarios WebCal.

En el escritorio, como uso Gnome, fue todo tan sencillo como agregar NextCloud al apartado de *Cuentas Online* de la *Configuración*. Con esto se sincroniza el calendario, las tareas, los contactos y se agrega el servidor como una unidad de red.

Para sincronizar los calendarios con Android, seguí las [instrucciones oficiales](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_android.html#contacts-and-calendar). Básicamente:

- Instalé [DAVx<sup>5</sup> desde F-Droid](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_android.html#contacts-and-calendar)
- Lo configuré con mi cuenta de NextCloud y, como mi móvil la única aplicación de calendario que tenía era GCal, me instalé también desde [F-Droid Etar](https://f-droid.org/es/packages/ws.xsoh.etar/)
### Tareas

![Tasks](tasks-1.png)

Siempre ando probando aplicaciones de listas de tareas y a todas les termino encontrando un "pero", generalmente que en cuanto las empiezo a usar de forma continuada me empiezan a hacer falta las "características premium".

La [aplicación de Tareas](https://apps.nextcloud.com/apps/tasks) me da lo que necesito: agrupar tareas en listas, crear subtareas, recordatorios, marcar prioridades, etc.

En Gnome uso [planify](https://github.com/alainm23/planify) y para Android uso [Tasks.org](https://f-droid.org/packages/org.tasks), un clon de Astrid. Para usarla sin necesidad de suscripción, recuerda instalarla desde F-Droid.
### Marcadores del navegador

![Bookmarks](Bookmarks.png)

La aplicación [Bookmarks](https://apps.nextcloud.com/apps/bookmarks) nos sirve para organizar y almacenar sitios web, por si sola, es un poco tedioso de mantener porque ni en sueños cuento con acordarme de agregar manualmente los marcadores de forma habitual. 

Pero, como para todo hay una aplicación, con [Floccus](https://floccus.org/) puedo sincronizar los marcadores de los equipos de escritorio con una [extensión para Firefox](https://addons.mozilla.org/en-US/firefox/addon/floccus/) y acceder a ellos y agregar nuevos con una [aplicación en Android](https://f-droid.org/en/packages/org.handmadeideas.floccus/). 

Por motivos técnicos, en Android no hay sincronización con el navegador y es necesario usar la aplicación, para guardar un enlace debemos usar la acción de compartir y seleccionar Floccus.

### Resumen:

En esta primera parte, hemos revisado las aplicaciones básicas de mi día a día. 

A continuación la tabla resumen con los enlaces a la tienda de aplicaciones de NextCloud, F-Droid para las aplicaciones de Android y las aplicaciones que utilizo en Linux, concretamente en el escritorio Gnome.

| Aplicación  | Nextcloud store                                        | F-Droid                                                                                                            | Gnome                                                                               |
| ----------- | ------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------- |
| Fotografías | [Memories](https://apps.nextcloud.com/apps/memories)   | [Memories](https://f-droid.org/es/packages/gallery.memories/)                                                      |                                                                                     |
| Calendario  | [Calendar](https://apps.nextcloud.com/apps/calendar)   | [DAVx](https://f-droid.org/packages/at.bitfire.davdroid/)<br>[Etar](https://f-droid.org/es/packages/ws.xsoh.etar/) | [Gnome Calendar](https://apps.gnome.org/es/Calendar/)                                                 |
| Tareas      | [Tasks](https://apps.nextcloud.com/apps/tasks)         | [Tasks.org](https://f-droid.org/packages/org.tasks)                                                                | [Planify](https://flathub.org/apps/io.github.alainm23.planify) (Flathub)            |
| Marcadores  | [Bookmarks](https://apps.nextcloud.com/apps/bookmarks) | [Floccus](https://f-droid.org/en/packages/org.handmadeideas.floccus/)                                              | [Floccus](https://addons.mozilla.org/en-US/firefox/addon/floccus/) (Firefox add-on) |






