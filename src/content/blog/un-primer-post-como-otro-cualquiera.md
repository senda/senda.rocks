---
titulo: Un primer post como otro cualquiera
categoria: general
descripcion: <p>El primer post de un blog, nunca sé si hacerlo épico con una declaración de intenciones potente y un pelín de ego, o si hacer un hola, mundo! simple y sencillo, dejarme de historias y empezar a publicar cosas útiles</p>
pubDate: 2024-04-30T20:30:00
tags:
  - presentación
---
Los que me tenéis que soportar en redes sociales, ya sabéis que este blog podría haberse llamado tranquilamente El Escorial, porque me ha costado mucho más de lo esperado tenerlo online.

Primero me lié con la parte técnica, probando cosas, haciendo inventos. Luego fue la parte visual, con los estilos y, al final, tampoco tenía muy claro como orientar el sitio: categorías, páginas, tono, temáticas...

Vamos, que me puse a hacer un blog porque soy una envidiosa y empecé la casa por el tejado. Niños, no intentéis esto en casa, es mucho mejor tener primero el contenido y luego, ya con la cabeza clara, ir montando la forma de transmitirlo.

Al final, aquí estoy, dispuesta a compartir con vosotros mis andanzas por los mundos de la informática. En este sitio os contaré cosas de software libre, desarrollo web, privacidad, aplicaciones útiles; a veces os daré mi opinión sobre cosas, pero dentro de estas líneas. La idea es que, entre todos, vayamos recuperando internet para la gente, dejando de depender de las corporaciones que mercadean con nuestros datos y nos sangran económicamente. 

Si te resulta interesante, puedes seguirme en Mastodon o suscribirte al feed RSS.