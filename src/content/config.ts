import { z, defineCollection, reference } from 'astro:content';

const blogCollection = defineCollection({ 
  type: 'content',
  schema: z.object({
    titulo: z.string(),
    categoria: reference('categoria'),
    descripcion: z.string(),
    pubDate: z.date(),
    tags: z.array(z.string()),
  })
});

const catCollection = defineCollection({
  type:'data',
  schema: z.object({
    categoria: z.string(),
    descripcion: z.string(),
  })
})

export const collections = {
    'blog': blogCollection,
    'categoria': catCollection,
  };